window.TicketMaster ||= {}
window.TicketMaster.init = ->
  window.TicketMaster.Data = {
    Events: new window.TicketMaster.Collections.Events(),
    Companies: new window.TicketMaster.Collections.Companies(),
    Stars: new window.TicketMaster.Collections.Stars(),
    Tickets: new window.TicketMaster.Collections.Tickets(),
    Venues: new window.TicketMaster.Collections.Venues(),
    Users: new window.TicketMaster.Collections.Users()
  }

  window.TicketMaster.Data.Events.query = new Parse.Query(window.TicketMaster.Models.Event);
  window.TicketMaster.Data.Companies.query = new Parse.Query(window.TicketMaster.Models.Company);
  window.TicketMaster.Data.Stars.query = new Parse.Query(window.TicketMaster.Models.Star);
  window.TicketMaster.Data.Tickets.query = new Parse.Query(window.TicketMaster.Models.Ticket);
  window.TicketMaster.Data.Venues.query = new Parse.Query(window.TicketMaster.Models.Venue);
  window.TicketMaster.Data.Users.query = new Parse.Query(window.TicketMaster.Models.User);

  window.TicketMaster.Data.Events.fetch()
  window.TicketMaster.Data.Companies.fetch()
  window.TicketMaster.Data.Tickets.fetch()
  window.TicketMaster.Data.Venues.fetch()
  window.TicketMaster.Data.Users.fetch()
  window.TicketMaster.Data.Stars.fetch()

  window.TicketMaster.State = new window.TicketMaster.AppState;
  window.AppView = new window.TicketMaster.Views.AppView;

window.TicketMaster.Models ||= {}
window.TicketMaster.Collections ||= {}

window.TicketMaster.AppState = Parse.Object.extend('AppState', defaults: filter: 'all')

window.TicketMaster.Models.Event = Parse.Object.extend('Event',
)

window.TicketMaster.Collections.Events = Parse.Collection.extend(
  model: window.TicketMaster.Models.Event
  company: ->
    window.TicketMaster.Data.Companies.get(@attributes["company"]["id"]).get('name')
  venue: ->
    window.TicketMaster.Data.Venues.get(@attributes["venue"]["id"]).get('name')
)

window.TicketMaster.Models.User = Parse.Object.extend('User',
)

window.TicketMaster.Collections.Users = Parse.Collection.extend(
  model: window.TicketMaster.Models.User
)

window.TicketMaster.Models.Company = Parse.Object.extend('Company',
)

window.TicketMaster.Collections.Companies = Parse.Collection.extend(
  model: window.TicketMaster.Models.Company
)

window.TicketMaster.Models.Star = Parse.Object.extend('Star',
)

window.TicketMaster.Collections.Stars = Parse.Collection.extend(
  model: window.TicketMaster.Models.Star

  currentUsers: ->
    if Parse.User.current()
      @filter (star) ->
        star.attributes["user"]["id"] == Parse.User.current().id
    else
      return null

  currentUsersMap: ->
    _.map window.TicketMaster.Data.Stars.currentUsers(), (num) ->
      num.get('event').id
)

window.TicketMaster.Models.Ticket = Parse.Object.extend('Ticket',
)

window.TicketMaster.Collections.Tickets = Parse.Collection.extend(
  model: window.TicketMaster.Models.Event

  forEvent: (id) ->
    @filter (num) ->
      num.attributes["event"]["id"] == id
)

window.TicketMaster.Models.Venue = Parse.Object.extend('Venue',
)

window.TicketMaster.Collections.Venues = Parse.Collection.extend(
  model: window.TicketMaster.Models.Event
)

window.TicketMaster.Views ||= {}

window.TicketMaster.Views.Session = Parse.View.extend(
  el: $("#user-link")
  template: _.template($('#login-template').html())
  events:
    'click #login': 'logIn'
    'click #logout': 'logOut'

  initialize: ->
    @$el.html _.template($('#login-template').html())

  logIn: ->
    zis = @
    Parse.FacebookUtils.logIn "public_profile,email",
      success: (user) ->
        FB.api '/me', (me) ->
          user.set 'nickname', me.name
          user.set 'email', me.email
          user.set 'facebookId', me.id
          user.save()
          return
        zis.$el.html _.template($('#login-template').html())
        window.AppView.addAll(window.TicketMaster.Data.Stars)
        $("#login-modal").modal("hide")
        _.map window.TicketMaster.Data.Stars.currentUsers(), (num) ->
          $(".favorite[data-id=" + num.get('event').id + "]").addClass("disabled");


        return
      error: (user, error) ->
        return

  logOut: ->
    zis = @
    Parse.User.logOut()
    zis.$el.html _.template($('#login-template').html())
    $(".disabled").removeClass("disabled")
)
window.TicketMaster.Views.AppView = Parse.View.extend(
  el: $('#app')
  events:
    'click .details': 'showDetails'
    'click #list-favorites' : 'showListFavorites'
    'click .favorite'  : 'favoriteEvent'
    'hidden.bs.modal #details-modal': 'removeHash'
    'keyup #search' : 'filterEvents'
    'click #categories-filter' : 'filterEventsByCategory'
    'click #loginModalLink' : "triggerLogin"

  triggerLogin: ->
    window.Login.logIn()

  filterEventsByCategory: (e) ->
    $("#categories-filter li").removeClass("active")
    $(e.target).parent().addClass("active")
    category = $(e.target).data("category")
    search = $("#search").val();
    if category != ""
      $("#current-category").html(s(category).humanize().value() + " <span class=\"caret\"></span>")
    else
      $("#current-category").html("All <span class=\"caret\"></span>")

    category = category.toLowerCase()
    if category == "all" and search == ""
      $(".content .thumbnail.event").parent().show()
    else
      $(".content .thumbnail.event").each (cur, item) ->
        if $(item).data("category").toLowerCase().indexOf(category) > -1
          if search == "" or $(item).data("title").toLowerCase().indexOf(search) > -1
            $(item).parent().show()
        else
          $(item).parent().hide()


  filterEvents: ->
    search = $("#search").val();
    category = $("#categories-filter li.active a").data("category").toLowerCase()
    if search == "" and category == "all"
      $(".content .thumbnail.event").parent().show()
    else
      $(".content .thumbnail.event").each (cur, item) ->
        if $(item).data("title").toLowerCase().indexOf(search) > -1
          if category == "all" or $(item).data("category").toLowerCase().indexOf(category) > -1
            $(item).parent().show()
        else
          $(item).parent().hide()


  removeHash: ->
    history.pushState(null, null, '#');
    document.title = "Ticket Master"

  favoriteEvent: (e) ->
    e.stopPropagation()
    e.preventDefault()
    $(e.currentTarget).addClass("disabled")
    if (!Parse.User.current())
      $("#login-modal").modal("show")
    else
      id = $(e.currentTarget).data("id")
      ev = window.TicketMaster.Data.Events.get(id)
      title = ev.get("title")
      u = Parse.User.current()

      current = new window.TicketMaster.Models.Star
      current.set({email: u.getEmail(), nickname: u.get("nickname"), event: ev, title: title, user: u})
      current.save()
      window.TicketMaster.Data.Stars.add(current)

      FB.ui {
        method: 'share'
        action_object: "event"
        action_type: 'og.likes'
        action_properties: JSON.stringify(object: 'http://ticketmaster.ognjen.io/#' + id)
      }, (response) ->


    return

  showListFavorites: (e) ->
    e.preventDefault()
    e.stopPropagation()
    if (!Parse.User.current())
      $("#login-modal").modal("show")
    else
      $("#favorites-modal").modal("show")

  showDetails: (e) ->
    e.stopPropagation()
    e.preventDefault()
    z = window.TicketMaster.Data.Events.get($(e.currentTarget).data("id"))
    window.AppView.showDetailsFor(z.id)

  showDetailsFor: (id) ->
    z = window.TicketMaster.Data.Events.get(id)
    to_render = new window.TicketMaster.Views.EventDetailView(model: z)
    $("#details-modal-content").html(to_render.render().el.innerHTML)
    $("#details-modal-title").html(z.attributes["title"] + " <small class='text-muted'>" + z.attributes['category'] + "</small>")
    $("#details-modal").modal('show')
    history.pushState(null, null, "#" + z.id);
    document.title = "Ticket Master | " + z.get("title")

  initialize: ->
    @render()

    window.TicketMaster.Data.Stars.bind('add', this.addOne)
    window.TicketMaster.Data.Stars.bind('reset', this.addAll)

    return

  addAll: (collection) ->
    $('#favorites-modal-content').html ''
    z = @
    _.each window.TicketMaster.Data.Stars.currentUsers(), window.AppView.addOne
    return

  addOne: (ez) ->
    ev = window.TicketMaster.Data.Events.get(ez.attributes.event.id)
    if ev
      view = new window.TicketMaster.Views.StarView(model: ev)
      $('#favorites-modal-content').append view.render().el
    return

  render: ->
    window.Eve = new window.TicketMaster.Views.EventsView
    window.Login = new window.TicketMaster.Views.Session
    return
)

window.TicketMaster.Views.Stream = Parse.View.extend(
  el: $("#stream")
  template: _.template($('#stream-template').html())
  initialize: ->
    a = _.map window.TicketMaster.Data.Stars.models, (num) ->
      ev = window.TicketMaster.Data.Events.get(num.attributes['event']['id'])
      return {timestamp: moment(num.createdAt), text: num.attributes['nickname'] + ' starred ' + ev.attributes['title']}
    b = _.map window.TicketMaster.Data.Events.models, (num) ->
      return {timestamp: moment(num.createdAt), text: num.attributes['title'] + ' event created.'}
    template = _.template($('#stream-template').html())
    @stream = [a, b]
    @stream = _.flatten @stream
    @stream = _.sortBy(@stream, 'timestamp');
    @stream = @stream.reverse()
    @$el.html template({stream: @stream})
)

window.TicketMaster.Views.EventsView = Parse.View.extend(
  el: '.content'
  initialize: ->
    self = this;
    #_.bindAll(this, 'addOne', 'addAll', 'addSome', 'render', 'toggleAllComplete', 'logOut', 'createOnEnter');

    @$el.html _.template($('#events-template').html())

    @eventz = new window.TicketMaster.Collections.Events
    @eventz.query = new Parse.Query(window.TicketMaster.Models.Event);

    @eventz.bind('add', this.addOne);
    @eventz.bind('reset', this.addAll);
    @eventz.bind('all', this.render);

    @eventz.fetch()

    return

  addCategories: ->
    categories = _.uniq(_.pluck(_.pluck(TicketMaster.Data.Events.toArray(), "attributes"), "category"))
    $("#categories-filter").html('<li class="active"><a href="#" data-category="">All</a></li>')
    _.each categories, (item) ->
      z = '<li><a href="#" data-category="' + item + '">' + s(item).humanize().value() + '</a></li>'
      $("#categories-filter").append z

  addAll: (collection) ->
    window.Eve.addCategories()
    window.Stream = new window.TicketMaster.Views.Stream
    $('#events-list').html ''
    z = @
    _.each z.models, window.Eve.addOne
    if (window.location.hash != "")
      window.AppView.showDetailsFor(window.location.hash.replace("#", ""))
    return

  addOne: (ez) ->
    try
      view = new window.TicketMaster.Views.EventView(model: ez)
      @$('#events-list').append view.render().el
    return

  render: ->
    return
)

window.TicketMaster.Views.EventView = Parse.View.extend(
  tagName: 'div'
  className: "event"
  template: _.template($('#event-template').html())

  initialize: ->
    @model = @options.model
    return

  render: ->
    to_pass = @model.toJSON()
    if @model.attributes["venue"]
      to_pass.venue = window.TicketMaster.Data.Venues.get(@model.attributes["venue"]["id"])

    if @model.attributes["company"]
      to_pass.company = window.TicketMaster.Data.Companies.get(@model.attributes["company"]["id"])

    to_pass.display_picture = @model.attributes["display_picture"].url()

    to_pass.disabled = ""
    if window.TicketMaster.Data.Stars.currentUsersMap().indexOf(@model.id) > -1
      to_pass.disabled = "disabled"

    $(@el).html @template(to_pass)
    this
)

window.TicketMaster.Views.StarView = Parse.View.extend(
  tagName: 'div'
  className: "event"
  template: _.template($('#fav-template').html())

  initialize: ->
    @model = @options.model
    return
  render: ->
    to_pass = @model.toJSON()
    if @model.attributes["venue"]
      to_pass.venue = window.TicketMaster.Data.Venues.get(@model.attributes["venue"]["id"])

    if @model.attributes["company"]
      to_pass.company = window.TicketMaster.Data.Companies.get(@model.attributes["company"]["id"])

    to_pass.display_picture = @model.attributes["display_picture"].url()

    to_pass.disabled = ""
    if window.TicketMaster.Data.Stars.currentUsersMap().indexOf(@model.id) > -1
      to_pass.disabled = "disabled"

    $(@el).html @template(to_pass)
    this
)

window.TicketMaster.Views.EventDetailView = Parse.View.extend(
  tagName: 'div'
  template: _.template($('#event-details-template').html())
  initialize: ->
    @model = @options.model
    return
  render: ->
    to_pass = @model.toJSON()
    if @model.attributes["venue"]
      to_pass.venue = window.TicketMaster.Data.Venues.get(@model.attributes["venue"]["id"])

    if @model.attributes["company"]
      to_pass.company = window.TicketMaster.Data.Companies.get(@model.attributes["company"]["id"])

    to_pass.display_picture = @model.attributes["display_picture"].url()
    $(@el).html @template(to_pass)
    this
)

# ---
# generated by js2coffee 2.1.0
